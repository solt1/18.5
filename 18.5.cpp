﻿#include <iostream>
#include <string>

class Player {
private:
    std::string name;
    int score;
public:
    Player() : name(""), score(0) {}
    Player(const std::string& name, int score) : name(name), score(score) {}
    void setName(const std::string& name) { this->name = name; }
    void setScore(int score) { this->score = score; }
    std::string getName() const { return name; }
    int getScore() const { return score; }
};

void sortPlayers(Player* players, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            if (players[i].getScore() < players[j].getScore()) {
                Player temp = players[i];
                players[i] = players[j];
                players[j] = temp;
            }
        }
    }
}

int main() {
    int numPlayers;
    std::cout << "Enter the number of players: ";
    std::cin >> numPlayers;
    Player* players = new Player[numPlayers];
    for (int i = 0; i < numPlayers; i++) {
        std::string name;
        int score;
        std::cout << "Enter player #" << i + 1 << " name: ";
        std::cin >> name;
        std::cout << "Enter player #" << i + 1 << " score: ";
        std::cin >> score;
        players[i] = Player(name, score);
    }
    sortPlayers(players, numPlayers);
    std::cout << "Player Rankings:" << std::endl;
    for (int i = 0; i < numPlayers; i++) {
        std::cout << players[i].getName() << ": " << players[i].getScore() << std::endl;
    }
    delete[] players;
    return 0;
}
